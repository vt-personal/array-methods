### Description

This project contains methods used to work with arrays in order to transform them as per the requirements.

### Project setup

1. Install Node.js version 18.18.2
2. Install Yarn version 1.22.19
3. Install node_modules by running:

```
yarn install
```

### Running the project

To test all the methods, run:

```
yarn test
```

You can give conditions to the methods by changing arraysOfNumbers in the \*.test.ts files.
