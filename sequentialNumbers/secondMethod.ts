export const checkSequentialNumbers = (arr: number[]) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== arr[0] + i) {
      return false;
    }
  }

  return true;
};
