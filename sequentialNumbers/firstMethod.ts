export const checkSequentialNumbers = (arr: number[]) => {
  arr = arr.sort((a, b) => a - b);
  return arr.every((num, index) => {
    return (
      index === 0 || num === arr[index - 1] + 1 || num === arr[index - 1] - 1
    );
  });
};
