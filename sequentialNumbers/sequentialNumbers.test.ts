import { checkSequentialNumbers as firstMethod } from "./firstMethod";
import { checkSequentialNumbers as secondMethod } from "./secondMethod";

describe("Test Sequential Numbers", () => {
  const arraysOfNumbers = [
    {
      values: [1, 2, 3, 4, 5],
      expected: true,
    },
    {
      values: [5, 4, 3, 2, 1],
      expected: true,
    },
    {
      values: [1, 2, 3, 4, 5, 6],
      expected: true,
    },
    {
      values: [6, 5, 4, 3, 2, 1],
      expected: true,
    },
    {
      values: [1, 3, 2, 4, 5],
      expected: true,
    },
    {
      values: [1, 2, 3, 4, 6],
      expected: false,
    },
    {
      values: [1, 2, 3, 4, 5, 7],
      expected: false,
    },
    {
      values: [1, 2, 3, 4, 5, 0],
      expected: true,
    },
    {
      values: [1, 2, 3, 4, 5, 8],
      expected: false,
    },
    {
      values: [-10, 1, 2, 3, 4, 5, 9],
      expected: false,
    },
    {
      values: [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1],
      expected: true,
    },
  ];
  it("check first method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(firstMethod(array.values)).toBe(array.expected);
    });
  });
  it("check second method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(secondMethod(array.values)).toBe(array.expected);
    });
  });
});
