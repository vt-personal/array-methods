import { getUniqueElements as firstMethod } from "./firstMethod";
import { getUniqueElements as secondMethod } from "./secondMethod";
import { getUniqueElements as thirdMethod } from "./thirdMethod";
import { getUniqueElements as fourthMethod } from "./fourthMethod";

describe("Test Sequential Numbers", () => {
  const arraysOfNumbers = [
    {
      values: [1, 2, 3, 4, 5],
      expected: [1, 2, 3, 4, 5],
    },
    {
      values: [1, 5, 7, 4, 4, 4, 2, 9, 10, 10],
      expected: [1, 5, 7, 4, 2, 9, 10],
    },
    {
      values: [1, 2, 3, 4, 5, 1, 2, 3, 4, 5],
      expected: [1, 2, 3, 4, 5],
    },
    {
      values: [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5],
      expected: [1, 2, 3, 4, 5],
    },
    {
      values: [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5],
      expected: [1, 2, 3, 4, 5],
    },
    {
      values: [
        1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4,
        5, 1, 2, 3, 4, 5,
      ],
      expected: [1, 2, 3, 4, 5],
    },
  ];
  it("check first method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(firstMethod(array.values)).toEqual(array.expected);
    });
  });
  it("check second method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(secondMethod(array.values)).toEqual(array.expected);
    });
  });
  it("check third method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(thirdMethod(array.values)).toEqual(array.expected);
    });
  });
  it("check fourth method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(fourthMethod(array.values)).toEqual(array.expected);
    });
  });
});
