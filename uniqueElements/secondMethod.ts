export const getUniqueElements = (arr: number[]) => {
  return arr.filter((value, index) => arr.indexOf(value) === index);
};
