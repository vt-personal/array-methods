export const getUniqueElements = (arr: number[]) => {
  return Array.from(new Set(arr));
};
