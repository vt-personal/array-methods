export const getUniqueElements = (arr: number[]) => {
  return arr.reduce((uniqueElements, value) => {
    if (!uniqueElements.includes(value)) {
      uniqueElements.push(value);
    }
    return uniqueElements;
  }, [] as number[]);
};
