import { countRepeatingNumbers as firstMethod } from "./firstMethod";
import { countRepeatingNumbers as secondMethod } from "./secondMethod";
import { countRepeatingNumbers as thirdMethod } from "./thirdMethod";

describe("Test Sequential Numbers", () => {
  const arraysOfNumbers = [
    {
      values: [1, 2, 3, 4, 5],
      expected: [
        {
          1: 1,
        },
        {
          2: 1,
        },
        {
          3: 1,
        },
        {
          4: 1,
        },
        {
          5: 1,
        },
      ],
    },
    {
      values: [1, 1, 1, 2, 5, 8, 3, 5, 5, 5, 4, 5],
      expected: [
        {
          1: 3,
        },
        {
          2: 1,
        },
        {
          5: 5,
        },
        {
          8: 1,
        },
        {
          3: 1,
        },
        {
          4: 1,
        },
      ],
    },

    {
      values: [
        -20, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 900, 120, 7800, 1200,
        -20, 10, -10,
      ],
      expected: [
        {
          "-20": 2,
        },
        {
          40: 11,
        },
        {
          900: 1,
        },
        {
          120: 1,
        },
        {
          7800: 1,
        },
        {
          1200: 1,
        },
        {
          10: 1,
        },
        {
          "-10": 1,
        },
      ],
    },
  ];

  it("check first method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(firstMethod(array.values)).toEqual(array.expected);
    });
  });
  it("check second method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(secondMethod(array.values)).toEqual(array.expected);
    });
  });
  it("check third method", () => {
    arraysOfNumbers.forEach((array) => {
      expect(thirdMethod(array.values)).toEqual(array.expected);
    });
  });
});
