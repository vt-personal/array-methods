export function countRepeatingNumbers(numbers: number[]) {
  const keys = Array.from(new Set(numbers));
  const countMap: { [key: string]: number } = {};

  numbers.forEach((num) => {
    countMap[num] = (countMap[num] || 0) + 1;
  });

  return keys.map((key) => ({ [key]: countMap[key] }));
}
