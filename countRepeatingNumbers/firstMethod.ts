export const countRepeatingNumbers = (arr: number[]) => {
  return arr.reduce((acc, value) => {
    const index = acc.findIndex((item) => item[value]);
    if (index === -1) {
      acc.push({ [value]: 1 });
    } else {
      acc[index][value]++;
    }

    return acc;
  }, [] as Record<number, number>[]);
};
