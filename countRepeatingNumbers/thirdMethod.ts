export const countRepeatingNumbers = (
  arr: number[]
): { [key: string]: number }[] => {
  const map = new Map<number, number>();

  for (const value of arr) {
    map.set(value, (map.get(value) || 0) + 1);
  }

  return Array.from(map.entries()).map(([key, value]) => ({
    [key.toString()]: value,
  }));
};
